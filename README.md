# FS transactions Client

Client for retrieving FS transactions



Installation
-------------

```
poetry shell
poetry install
```


Client usage
-------------
See `example-client-usage.py`


URLs
-------------

Test api url: `https://fs-test.uio.no/fsrest/rest/hovedbok`
Prod api url: `https://fsws.usit.no/fsrest/rest/hovedbok`


Running tests
-------------

```
pytest
```
