from fs_transactions_client import FsTransactionsClient
from fs_transactions_client.models import Batch
from pprint import pprint
from datetime import datetime

test_headers = {
    "X-Gravitee-API-Key": "",
    "Accept": "application/json",
}  # gravitee test api token

client = FsTransactionsClient(
    url="https://gw-uio.intark.uh-it.no/fsdemo-api-rest/", headers=test_headers
)

# date = datetime.now().isoformat()

response = client.get_batch(dato="20210309", ny_beregning=True)

print("Got reponse:")
pprint(response)
pprint(response.status_code)  # Check response statuscode

print("Got json:")
batch_json = response.json()
pprint(batch_json)

if response.status_code == 200:
    print("Converted to Batch object:")
    b = Batch.from_dict(batch_json)
    print(f"Got ", len(b.bilagliste), " bilag")
    pprint(b)
