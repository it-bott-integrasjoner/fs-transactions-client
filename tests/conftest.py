import json
import os
import pytest
from fs_transactions_client import FsTransactionsClient
from fs_transactions_client.client import FsTransactionsEndpoints


def load_json_file(name):
    here = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
    with open(os.path.join(here, "fixtures", name)) as f:
        data = json.load(f)
    return data


@pytest.fixture
def baseurl():
    return "https://localhost"


@pytest.fixture
def endpoints(baseurl):
    return FsTransactionsEndpoints(baseurl)


@pytest.fixture
def batch_url(baseurl):
    return FsTransactionsEndpoints(baseurl).batch()


@pytest.fixture
def fsclient(baseurl):
    return FsTransactionsClient(baseurl)


@pytest.fixture
def batch():
    return load_json_file("batch.json")


@pytest.fixture
def transaction():
    return load_json_file("transaction.json")
