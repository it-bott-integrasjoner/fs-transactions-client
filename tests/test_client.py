import pytest
import requests
from pip._vendor.requests import Response
from typing import Any, Dict

from fs_transactions_client import FsTransactionsClient
from fs_transactions_client.client import FsTransactionsEndpoints


@pytest.fixture
def header_name():
    return "X-Test"


@pytest.fixture
def client(header_name):
    class TestClient(FsTransactionsClient):
        default_headers = {
            header_name: "6a9a32f0-7322-4ef3-bbce-6685a3388e67",
        }

    return TestClient


def test_init_applies_default_headers(client, baseurl, header_name):
    headers: Dict[str, Any] = {}
    client = client(baseurl, headers=headers)
    assert header_name in client.headers
    assert client.headers[header_name] == client.default_headers[header_name]


def test_init_modify_default_header(client, baseurl, header_name):
    headers = {header_name: "ede37fdd-a2ae-4a96-9d80-110623425ea6"}
    client = client(baseurl, headers=headers)
    # Check that we respect the headers arg, and don't use default_headers
    assert client.headers[header_name] == headers[header_name]
    # Check that we don't do this by mutating default_headers
    assert client.default_headers[header_name] != headers[header_name]


# Check for correct request body
def default_params_matcher(request: requests.PreparedRequest):
    if (
        request.method == "POST"
        and request.body == "dato=20200125&beregning=Ny&oppdaterFS=N"
    ):
        return True
    # otherwise, it will trigger a 403 (if it had just returned false, it would trigger "NoMockAddress", which is not very useful...
    resp = Response()
    resp.status_code = 403
    resp.raise_for_status()


def matcher(request: requests.PreparedRequest):
    if (
        request.method == "POST"
        and request.body == "dato=20200125&beregning=Tidligere&oppdaterFS=J"
    ):
        return True
    # otherwise, it will trigger a 403 (if it had just returned false, it would trigger "NoMockAddress", which is not very useful...
    resp = Response()
    resp.status_code = 403
    resp.raise_for_status()


# Test post method


def test_successful_get_batch_from_fs(fsclient, requests_mock, baseurl, batch):
    """A working GET call should return HTTP 200, with json content"""
    url = FsTransactionsEndpoints(baseurl).batch()
    requests_mock.post(
        url, json=batch, status_code=200, additional_matcher=default_params_matcher
    )

    response = fsclient.get_batch(dato="20200125")
    assert response.status_code == 200
    assert response.json() == batch


def test_successful_get_batch_from_fs_with_params(
    fsclient, requests_mock, baseurl, batch
):
    """A working GET call should return HTTP 200, with json content"""
    url = FsTransactionsEndpoints(baseurl).batch()
    requests_mock.post(
        url,
        json=batch,
        status_code=200,
        complete_qs=True,
        additional_matcher=matcher,
        request_headers={"Content-Type": "application/x-www-form-urlencoded"},
    )

    response = fsclient.get_batch(dato="20200125", ny_beregning=False, oppdater_fs=True)
    assert response.status_code == 200
    assert response.json() == batch


def test_failing_to_get_batch(fsclient, requests_mock, baseurl):
    """A failing GET all vouchers call should still return json"""
    url = FsTransactionsEndpoints(baseurl).batch()
    requests_mock.post(
        url,
        json={"error": "some json error message"},
        complete_qs=True,
        additional_matcher=default_params_matcher,
        status_code=404,
    )

    response = fsclient.get_batch(dato="20200125")
    assert response.json() == {"error": "some json error message"}


def batch_nrmatcher(request: requests.PreparedRequest):
    if (
        request.method == "POST"
        and "buntnr=F20200125" in request.body  # type: ignore[operator]
        and "&beregning=Tidligere" in request.body  # type: ignore[operator]
        and "&statusFortHovedBok=N" in request.body  # type: ignore[operator]
        and "&oppdaterFS=N" in request.body  # type: ignore[operator]
    ):
        return True
    # otherwise, it will trigger a 403 (if it had just returned false, it would trigger "NoMockAddress", which is not very useful...
    resp = Response()
    resp.status_code = 403
    resp.raise_for_status()


def test_successful_get_batch_with_batchnr(fsclient, requests_mock, baseurl, batch):
    """A working GET call should return HTTP 200, with json content"""
    url = FsTransactionsEndpoints(baseurl).batch()
    requests_mock.post(
        url,
        json=batch,
        status_code=200,
        complete_qs=True,
        additional_matcher=batch_nrmatcher,
        request_headers={"Content-Type": "application/x-www-form-urlencoded"},
    )

    response = fsclient.get_batch(
        buntnr="F20200125",
        ny_beregning=False,
        oppdater_fs=False,
        status_fort_hovedbok=False,
    )
    assert response.status_code == 200
    assert response.json() == batch


def test_exception_when_getting_batch_without_batchnr_or_date(
    fsclient, requests_mock, baseurl, batch
):
    """A working GET call should return HTTP 200, with json content"""
    url = FsTransactionsEndpoints(baseurl).batch()
    requests_mock.post(url, json=batch, status_code=200)

    with pytest.raises(ValueError) as err:
        fsclient.get_batch()

    assert err.type == ValueError
