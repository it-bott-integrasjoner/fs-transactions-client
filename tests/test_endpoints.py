from fs_transactions_client.client import FsTransactionsEndpoints


def test_init(baseurl):
    endpoints = FsTransactionsEndpoints(baseurl)
    assert endpoints.baseurl == baseurl


def test_init_batchurl(baseurl):
    endpoints = FsTransactionsEndpoints(baseurl)
    assert endpoints.batch_url == "fsrest/rest/okonomi/hovedbok"
