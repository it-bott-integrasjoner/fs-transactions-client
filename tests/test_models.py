from fs_transactions_client.models import Batch, Transaction
from fs_transactions_client.models import parse_date_string


def test_transaction(transaction):
    model = Transaction(**transaction)
    assert model.bilagsnr == transaction["bilagsnr"]
    assert model.artnr == transaction["artnr"]
    assert model.stednr == transaction["stednr"]
    assert model.tiltaknr == transaction["tiltaknr"]
    assert model.dato == parse_date_string(transaction["dato"])
    assert model.tekst == transaction["tekst"]
    assert model.belop == transaction["belop"]
    assert model.kdim1 == transaction["kdim1"]
    assert model.kdim2 == transaction["kdim2"]
    assert model.kdim3 == transaction["kdim3"]
    assert model.kdim4 == transaction["kdim4"]
    assert model.kdim5 == transaction["kdim5"]
    assert model.kdim6 == transaction["kdim6"]


def test_batch(batch):
    model = Batch(**batch)
    assert model.buntnr == batch["buntnr"]
    assert model.antallrader == batch["antallrader"]
    assert isinstance(model.bilagliste, list)
