"""Models used by the client"""
import datetime
import json
from typing import Any, List, Dict, Optional, Type, TypeVar

import pydantic
from pydantic import validator

T = TypeVar("T", bound="BaseModel")


class BaseModel(pydantic.BaseModel):
    """Expanded BaseModel for convenience"""

    @classmethod
    def from_dict(cls: Type[T], data: Dict[Any, Any]) -> T:
        """Initialize class from dict"""
        return cls(**data)

    @classmethod
    def from_json(cls: Type[T], json_data: str) -> T:
        """Initialize class from json file"""
        data = json.loads(json_data)
        return cls.from_dict(data)


def parse_date_string(date_string: str) -> datetime.date:
    """
    Parses a string with date (format: yyyyMMDD), into datetime.date
    """
    return datetime.datetime.strptime(date_string, "%Y%m%d").date()


class Transaction(BaseModel):
    """Model representing a bilag/transaction"""

    bilagsnr: str
    artnr: Optional[int]
    stednr: Optional[int]
    tiltaknr: Optional[int]
    dato: datetime.date

    @validator("dato", pre=True)
    def _parse_datetime(cls, value: str) -> datetime.date:
        if len(value) != 8 and not value.isnumeric():
            raise ValueError("dato field is wrong format.")
        return parse_date_string(value)

    tekst: str
    belop: int
    kdim0: str
    kdim1: Optional[str]
    kdim2: Optional[str]
    kdim3: Optional[str]
    kdim4: Optional[str]
    kdim5: Optional[str]
    kdim6: Optional[str]


class Batch(BaseModel):
    """Model representing a bunt/batch, with a list of bilag/transactions"""

    buntnr: str
    sumnegbelop: int
    sumposbelop: int
    antallrader: int
    bilagliste: List[Transaction]
