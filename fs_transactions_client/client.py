"""Client for connecting to FsTransactions API"""
import logging
import urllib.parse
from types import ModuleType
from typing import Any, Dict, Optional, Union
from fs_transactions_client.utils import merge_dicts
import requests

logger = logging.getLogger(__name__)

JsonType = Any


class FsTransactionsEndpoints:
    def __init__(
        self,
        url: str,
        batch_url: str = "fsrest/rest/okonomi/hovedbok",
    ):
        self.baseurl = url
        self.batch_url = batch_url

    """ Get endpoints relative to the FsTransactions API URL. """

    def __repr__(self) -> str:
        return "{cls.__name__}({url!r})".format(cls=type(self), url=self.baseurl)

    def batch(self) -> str:
        """
        URL for Batch endpoint
        """
        return urllib.parse.urljoin(self.baseurl, self.batch_url)


class FsTransactionsClient(object):
    default_headers = {
        "Accept": "application/json",
    }

    def __init__(
        self,
        url: str,
        headers: Union[None, Dict[str, Any]] = None,
        return_objects: bool = True,
        use_sessions: bool = True,
    ):
        """
        FsTransactions API client.

        :param str url: Base API URL
        :param dict headers: Append extra headers to all requests
        :param bool return_objects: Return objects instead of raw JSON
        :param bool use_sessions: Keep HTTP connections alive (default True)
        """

        self.urls = FsTransactionsEndpoints(url)
        self.headers = merge_dicts(self.default_headers, headers)
        self.return_objects = return_objects
        self.session: Union[ModuleType, requests.Session]
        if use_sessions:
            self.session = requests.Session()
        else:
            self.session = requests

    def _build_request_headers(
        self, headers: Optional[Dict[str, Any]]
    ) -> Dict[str, Any]:
        request_headers = {}
        for h in self.headers:
            request_headers[h] = self.headers[h]
        if headers is not None:
            for h in headers or ():
                request_headers[h] = headers[h]
        return request_headers

    def call(
        self,
        method_name: str,
        url: str,
        headers: Optional[Dict[str, Any]] = None,
        params: Optional[Dict[str, Any]] = None,
        return_response: bool = True,
        **kwargs: Any,
    ) -> Union[requests.Response, JsonType]:
        headers = self._build_request_headers(headers)
        if params is None:
            params = {}
        logger.debug(
            "Calling %s %s with params=%r",
            method_name,
            urllib.parse.urlparse(url).path,
            params,
        )
        r = self.session.request(
            method_name, url, headers=headers, params=params, **kwargs
        )
        if r.status_code in (500, 400, 401):
            logger.warning("Got HTTP %d: %r", r.status_code, r.content)
        if return_response:
            return r
        r.raise_for_status()
        return (
            r.json()
        )  # Note: krasjer her, dersom man får text og ikke json i requesten, og return_response=false

    def get(self, url: str, **kwargs: Any) -> requests.Response:
        return self.call("GET", url, **kwargs)

    def post(self, url: str, **kwargs: Any) -> requests.Response:
        return self.call("POST", url, **kwargs)

    def get_batch(
        self,
        dato: Optional[str] = None,
        ny_beregning: bool = True,
        buntnr: Optional[str] = None,
        oppdater_fs: bool = False,
        status_fort_hovedbok: Optional[bool] = None,
    ) -> requests.Response:
        """
        GETs one batch, with one or more transactions from FsTransactions api

        :param dato: Date string on format: yyyyMMdd. Last date that the report should contain.
            Often equal to todays date, or blank if this is not the first calculation.
        :param buntnr: Batch number to use, when getting old calculation, or None for the first calculation.
        :param ny_beregning: New or old calculation. Can be used to recreate earlier calculations

        Calls the fs api using POST with form urlencoded (according to spec)

        :returns response object. Get the json content with .json()
        """

        if dato is None and buntnr is None:
            raise ValueError(
                "Both dato and buntnr parameters are missing. Cannot continue."
            )

        url = self.urls.batch()

        headers = {"Content-Type": "application/x-www-form-urlencoded"}

        params = {
            "dato": dato,
            "buntnr": buntnr,
            "beregning": "Ny" if ny_beregning is True else "Tidligere",
            "oppdaterFS": "N" if oppdater_fs is False else "J",
        }

        if isinstance(status_fort_hovedbok, bool):
            params["statusFortHovedBok"] = "N" if status_fort_hovedbok is False else "J"

        return self.post(url, headers=headers, data=params)


def get_client(config_dict: Dict[str, Any]) -> FsTransactionsClient:
    """
    Get a FsTransactionsClient from configuration.
    """
    return FsTransactionsClient(**config_dict)
